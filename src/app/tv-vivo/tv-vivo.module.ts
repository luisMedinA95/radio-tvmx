import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TvVivoPageRoutingModule } from './tv-vivo-routing.module';

import { TvVivoPage } from './tv-vivo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TvVivoPageRoutingModule
  ],
  declarations: [TvVivoPage]
})
export class TvVivoPageModule {}
