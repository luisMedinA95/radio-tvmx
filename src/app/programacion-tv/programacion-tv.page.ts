import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-programacion-tv',
  templateUrl: './programacion-tv.page.html',
  styleUrls: ['./programacion-tv.page.scss'],
})
export class ProgramacionTvPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  ruta_tv = "https://radioytvmexiquense.mx/index.php/television/programacion/";
  ruta:any;
  constructor(private route: Router,
    private http: HttpClient, private menu: MenuController, private dom: DomSanitizer) {
     }

  ngOnInit() {
    this.ruta = this.dom.bypassSecurityTrustResourceUrl(this.ruta_tv);
    this.menu.enable(true);
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
    }, 7500);
  }

  home(){
    this.route.navigate(['/home']);
    this.menu.enable(true);
  }
}
