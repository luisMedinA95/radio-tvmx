import { Component, Input } from '@angular/core';
import { Platform, NavController, MenuController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FCM, NotificationData } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @Input() texto = 'Cargando...';
  loader = false;
  titulo_notificacion;
  cuerpo_notificacion;

  public Icns = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    }
  ];
  valor;
  enlace;
  notific;
  constructor(private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar,
    private navCtrl: NavController, private http: HttpClient, private menu: MenuController, private route: Router,
    private toastCtrl: ToastController, private fcm: FCM) {
    this.initializeApp();

    this.fcm.getToken().then(token => {
      this.valor = token;
      console.log("Token: ",this.valor);
    });
    
  
      this.fcm.onNotification().subscribe((data: NotificationData) => {   //NotificationData
        localStorage.setItem('traer',JSON.stringify(data));
        console.log("Obtengo Data:",data);
        //localStorage.setItem('traer',JSON.stringify(data));
        //console.log("Obtengo cadena: ", JSON.stringify(data));
        if (!data.wasTapped) {
          /*localStorage.setItem('link_not',data.body);
          console.log("Obtengo If:",data);
          console.log("If data: ",JSON.stringify(data));
          this.titulo_notificacion = JSON.stringify(data.title);
          this.cuerpo_notificacion = JSON.stringify(data.body);
          this.alerta_notificacion(data.title);*/
          var expresion = /http([^"'\s]+)/g
          this.enlace = data.body;
          const url = this.enlace.match(expresion);
          const dato = url[0];
          localStorage.setItem('link_not',dato);
          this.alerta_notificacion(data.title);
        } else {
          console.log("Obtengo else:",data);
          this.notific= '4';
          localStorage.setItem('llega',JSON.stringify(data));
        }
      }, error => {
        console.error("Error in notification", error)
      }
    );

    const inic= localStorage.getItem('active');
    if(inic === '2'){  
      this.navCtrl.navigateForward('/register');
    }else{
      if(localStorage.getItem('traer') != null){
        
        this.navCtrl.navigateForward('/notificaciones');
        localStorage.removeItem('traer');
      }else{
        localStorage.removeItem('traer');
        if(inic === '5'){
          this.navCtrl.navigateForward('/home');
          const not = localStorage.getItem('noti');
          console.log("Que traes aqui: ",not);
          const appcer = localStorage.getItem('llega');
          console.log("App cerrada: ",appcer);
          localStorage.removeItem('traer');
        }else{
          this.navCtrl.navigateForward('/register');
          localStorage.removeItem('traer');
        }
      }
    }
  }

  async alerta_notificacion(title) { //title
    const toast = await this.toastCtrl.create({
      message: 'Notificacion:' + title,
      duration: 30000,
      position: "bottom",
      cssClass: 'toas_notif',
      //showCloseButton: true,
      //closeButtonText: "Aceptar"
      buttons: [
        {
          side: 'start',
          //icon: 'star',
          text: 'Aceptar',
          handler: () => {
            //this.route.navigate(['/notificaciones']);
            this.route.navigate(['/notificaciones']);
            //localStorage.removeItem('traer');
          }
        }, {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        }
      ]
    });
    toast.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      //this.statusBar.styleBlackOpaque();
      //this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();
    });
  }
 
  edit;
  update_perfil(){
    this.route.navigate(['/perfil']);
    this.menu.enable(false);
  }
  dato;
  close(){
    this.responseDesactiveNotificacion().subscribe(solicitud=>{
      //alert("Me sali en Menu: "+JSON.stringify(solicitud));
      setTimeout(() => {
        this.loader = false;
        this.texto = 'Cargando...';
        this.route.navigate(['/register']);
        this.menu.enable(false);
        this.dato = 2;
        localStorage.setItem('active',this.dato);
      }, 3000);
    })
  }

  responseDesactiveNotificacion(): Observable<any>{
    this.loader = true;
    var usuario_logeado = JSON.parse(localStorage.getItem('logeado'));
    var request = {
      userAppId: usuario_logeado.userAppId,
      pushActive: false
    }
    const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
    const json = JSON.stringify(request)
    console.log("Mando cerrar push: ",json)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(dir, json, httpOptions);
  }
  exit(){
    this.menu.close();
  }
  Tv_vivo(){
    this.route.navigate(['/tv-vivo']);
    this.menu.enable(false);
  }
  Programacion_tv(){
    this.route.navigate(['/programacion-tv']);
  }
  Radio(){
    this.route.navigate(['/radio']);
  }
  Noticias(){
    this.route.navigate(['/noticias']);
  }

}
